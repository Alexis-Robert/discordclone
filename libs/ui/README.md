# UI

## Composants

<https://ui.shadcn.com/docs/components/>

## Utilisations

Ce package utilise les composants tailwindcss de [shadcn/ui](https://ui.shadcn.com/docs);

Générer un composant dans le package :

```bash
nx g @nx/next:component <component> --project=ui
```

Générer un composant avec **shadcn** :

```bash
npx shadcn-ui@latest add <component>
```

Le composant sera ensuite généré à la racine du workspace dans le dossier `@`.

Il suffit ensuite le copier/coller dans le dossier `/ui/lib/<component>/<component>` généré avec la première commande.

## Installation

<https://ui.shadcn.com/docs/installation/next>

## Configuration

La configuration se trouve dans le fichier `components.json` à la racine du workspace.

## Tests

Run `nx test ui` to execute the unit tests via [Jest](https://jestjs.io).
