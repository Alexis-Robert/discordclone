# Serveur

## Technologies principales

- [NestJS](https://nestjs.com/)
- [TypeScript](https://www.typescriptlang.org/)
- [Prisma](https://www.prisma.io/)
- [Socket.io](https://socket.io/)
- [tRPC](https://trpc.io/)

## Démarrer l'application web

```bash
# A la racine du workspace
nx serve server
```
