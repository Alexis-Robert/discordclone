# Web

## Technologies principales

- [Next.js](https://nextjs.org/)
- [TypeScript](https://www.typescriptlang.org/)
- [TailwindCSS](https://tailwindcss.com/)
- [Shadcn/ui](https://ui.shadcn.com/)
- [React-query](https://react-query.tanstack.com/)
- [Socket.io](https://socket.io/)
- [tRPC](https://trpc.io/)
- [Zustand](https://zustand-demo.pmnd.rs)

## Démarrer l'application web

```bash
# A la racine du workspace
nx serve web
```
